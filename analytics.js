$(function () {

  var Window = {
    addSlash: function (str) {
      return str.replace(/(?!\/)(.)$/, '$1/');
    },
    getQueryParams: function (qs) {
      // http://stackoverflow.com/questions/439463
      qs = qs.split('+').join(' ');
      var params = {}, tokens, re = /[?&]?([^=]+)=([^&]*)/g;
      while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
      }
      return params;
    },
    currentURL: function () {
      var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
      return this.addSlash(url);
    },
    currentHost: function () {
      return this.addSlash(window.location.protocol + '//' + window.location.host);
    },
    currentDir: function () {
      return this.currentURL().substring(0, this.currentURL().replace(/\/$/, '').lastIndexOf('/'));
    },
    GET: function (key) {
      var GET = this.getQueryParams(document.location.search);
      if (key) {
        return GET[key];
      } else {
        return GET;
      }
    },
    // Depreciated
    getVar: function (name) {
      var GET = this.GET();
      return GET[name];
    },
    getVarBool: function (name) {
      var variable = this.GET(name);
      if (typeof variable == 'undefined') {
        return null;
      } else {
        return !!parseInt(variable);
      }
    }
  };

//  var serverURL = 'http://115.146.94.208/couch/tweets';
//  var serverURL = 'http://localhost/couch/tweets';
//  var serverURL = 'http://localsite.com:5984/tweets';

  // This allows setting the server with a GET variable
  var serverURL = Window.GET('server') || Window.currentHost() + 'couch/tweets';
  // Whether CouchDB should regenerate stale views.
  var stale = Window.GET('stale') || '';
  var staleQuery = (stale ? '&stale=' + (stale + '') : '');
  var location = [-32.928101, 151.772003];
  // This must have lowest coords first.
  var bbox = [
    [-32.985149, 151.618805],
    [-32.854431, 151.810196]
  ];
  var bboxdsPoly = [
    [-32.854431, 151.810196],
    [-32.985149, 151.810196],
    [-32.985149, 151.618805],
    [-32.854431, 151.618805]
  ];
  bboxdsPoly[bboxdsPoly.length] = bboxdsPoly[0];

  var dayMap = {
    'Mon': 0,
    'Tue': 1,
    'Wed': 2,
    'Thu': 3,
    'Fri': 4,
    'Sat': 5,
    'Sun': 6
  };
  var hoursInDay = 24;
  var dayHourValue = function (dayName, hourNumber) {
    return dayMap[dayName] * hoursInDay + parseInt(hourNumber);
  };

  var requestView = function (view, success, error) {
    return $.getJSON(serverURL + '/_design/queries/_view/' + view + '?group=true' + staleQuery, success, error);
  };

  var requestViewUngrouped = function (view, success, error) {
    return $.getJSON(serverURL + '/_design/queries/_view/' + view + '?group=false' + staleQuery, success, error);
  };

  var normalParams = function (list) {
    var r = {mean: 0, stdev: 0};
    var len = 0, total = 0;
    list.forEach(function (value) {
      len += 1;
      total += value;
    });
    r.mean = total / len;
    var squares = 0;
    list.forEach(function (value) {
      squares += Math.pow(value - r.mean, 2);
    });
    r.stdev = Math.sqrt(squares / len);
    return r;
  };

  var pickTopN = function (count, items, key) {
    // Select the top count items from the list based on the value
    // associated with key.
    var winners = []; // Array of the top n things
//    console.log('picktop ' + count + ' from', items);

    for (var i = 0; i < count; i++) {
      winners[i] = {}; // NOTE: Assumes no values < 0 in items
      winners[i][key] = 0;
    }

    items.map(function (item) {
      for (var i = count - 1; i >= 0; i--) {
        if (winners[i][key] < item[key]) {
          for (var j = 1; j <= i; j++) {
            winners[j - 1] = winners[j];
          }
          winners[i] = item;
          break;
        }
      }

    });
//    console.log('returning', winners);
    return winners;
  };

  var normalFunc = function (x, mean, stdev) {
    var ret;
    ret = 1 / (stdev * Math.sqrt(2 * Math.PI));
    var exponent = -1 * Math.pow(x - mean, 2) / (2 * Math.pow(stdev, 2));
    ret *= Math.exp(exponent);
    return ret;
  };

  var normalDistDataPoints = function (values) {
    var distribution = normalParams(values);
    console.log(distribution);

    var min, max, numpoints, stdevs = 4, increment;
    min = distribution.mean - stdevs * distribution.stdev;

    if (min < 0) {
      min = 0;
    }

    max = distribution.mean + stdevs * distribution.stdev;
    numpoints = 2 * stdevs * 20; // 20 data points per stdev
    increment = (max - min) / numpoints;
    var datapoints = [];
    var x = min;
    while (x < max) {
      var y = normalFunc(x, distribution.mean, distribution.stdev);
      datapoints.push({x: x, y: y});
      x += increment;
    }

    return datapoints;
  };

  var toTitleCase = function (str) {
    // summary:
    //      Converts a string to Title Case.
    var parts = str.split(/\s+/);
    var title = '';
    for (var i = 0; i < parts.length; i++) {
      var part = parts[i];
      if (part != '') {
        title += part.slice(0, 1).toUpperCase() + part.slice(1, part.length);
        if (i != parts.length - 1 && parts[i + 1] != '') {
          title += ' ';
        }
      }
    }
    return title;
  };

  var transformKeyValueData = function (data) {
    var transformed = {
      values: []
    };
    data.rows.forEach(function (row) {
      transformed.values.push({
        label: row.key + '',
        value: row.value
      });
    });
    return [transformed];
  };

  var transformLineData = function (data) {
    var transformed = {values: []};
    data.rows.forEach(function (row) {
      var point = {
        x: row.key,
        y: row.value
      };
      transformed.values.push(point);
    });
    transformed.values.sort(function (a, b) {
      if (a.x == b.x) {
        return 0;
      } else if (a.x > b.x) {
        return 1
      } else {
        return -1;
      }
    });
    return [transformed];
  };

  var defaultArgs = {
    addTitle: function (svg, title) {
      var $svg = $(svg[0]);
      $svg.parent().prepend('<div class="chart-title">' + title + '</div>');
    }
  };

  var drawBarChart = function (element, data, args) {
    args = $.extend({}, defaultArgs, args);
    var processed = args.processed || false;

    var $element = $(element);
    var padding = args.padding || 15, height = args.height || 400;

    nv.addGraph(function () {
      var chart = nv.models.discreteBarChart()
          .x(function (d) {
            return d.label
          })
          .y(function (d) {
            return d.value
          })
          .staggerLabels(true)
          .tooltips(false)
          .width($element.width() - padding * 2)
          .height(height)
          .showValues(true);

      chart.yAxis.tickFormat(d3.format('i'));
      chart.xAxis.tickFormat(function (tick) {
        return toTitleCase(tick);
      });

      args.construct && args.construct(chart);

      if (processed) {
        data = [data];
      } else {
        data = transformKeyValueData(data);
      }

      console.error('data', data);
      console.error('width', $element.width());
      d3.select(element)
          .datum(data)
          .style('padding', padding + 'px')
          .transition().duration(500)
          .attr('height', height)
          .call(chart);

      nv.utils.windowResize(chart.update);
      return chart;
    }, args.complete);
  };

  var drawHorizontalBarChart = function (element, data, args) {
    args = $.extend({}, defaultArgs, args);
    var processed = args.processed || false;

    var $element = $(element);
    var padding = args.padding || 15, height = args.height || 400;

    nv.addGraph(function () {
      var chart = nv.models.multiBarHorizontalChart()
          .x(function (d) {
            return d.label
          })
          .y(function (d) {
            return d.value
          })
          .tooltips(false)
          .width($element.width() - padding * 2)
          .height(height)
          .showValues(true);

      chart.yAxis.tickFormat(d3.format('i'));
      chart.xAxis.tickFormat(function (tick) {
        return toTitleCase(tick);
      });

      args.construct && args.construct(chart);

      if (processed) {
        data = [data];
      } else {
        data = transformKeyValueData(data);
      }

      console.error('data', data);
      console.error('width', $element.width());
      d3.select(element)
          .datum(data)
          .style('padding', padding + 'px')
          .transition().duration(500)
          .attr('height', height)
          .call(chart);

      nv.utils.windowResize(chart.update);
      return chart;
    }, args.complete);
  };

  var drawPieChart = function (element, data, args) {
    args = args || {};
    var processed = args.processed || false;
    var donut = args.donut || false;

    nv.addGraph(function () {
      var chart = nv.models.pieChart()
          .x(function (d) {
            return d.label
          })
          .y(function (d) {
            return d.value
          })
          .showLabels(true);

      if (processed) {
        data = [data];
      } else {
        data = transformKeyValueData(data);
      }
      if (donut) {
        chart.donut(true);
      }

      console.log('data', data);
      d3.select(element)
          .datum(data)
          .transition().duration(500)
          .call(chart);
      nv.utils.windowResize(chart.update);
      return chart;
    });
  };

  var drawLineChart = function (element, data, args) {
    args = $.extend({}, defaultArgs, args);
    // Data format:
    //              {values:[{x:0, y:0}, {x:1, y:1}], key: 'Linear', color: '#ff7f0e'
    var chart;
    if (args.legend) {
      chart = nv.models.lineChart().showLegend(true);
    } else {

      chart = nv.models.lineChart().showLegend(false);
    }
    var $element = $(element);
    var padding = args.padding || 15, height = args.height || 400;

    chart = chart
        .width($element.width() - padding * 2)
        .height(height);

    var xLabel = args.xLabel || '';
    var yLabel = args.yLabel || '';
    var processed = args.processed || false;

    console.log('data', data);

    if (processed) {
      if (!(data instanceof Array)) {
        data = [data];
      }
    } else {
      data = transformLineData(data);
    }

    // Sort all series.
    data.forEach(function (series) {
      series.values.sort(function (a, b) {
        return a.x - b.x;
      })
    });

    console.error('data', data);

    chart.xAxis.axisLabel(xLabel);
    chart.yAxis
        .axisLabel(yLabel)
        .tickFormat(d3.format('i'));
    args.construct && args.construct(chart);

    var svg = d3.select(element);
    if (data[0]) {
      data[0].key = data[0].key || '';
    }
    svg.datum(data)
        .transition()
        .duration(500)
        .style('padding', padding + 'px')
        .attr('height', height);
    args.draw && args.draw(svg);
    svg.call(chart);

    nv.utils.windowResize(function () {
      d3.select(element).call(chart);
    });

    return chart;
  };

  // Analytics

//  requestView('days', function (result) {
//    console.log('days', result);
//  });

  var nameToID = function (name) {
    return name.toLowerCase().replace(/\s/, '-');
  };

  var tweetYAxis = function (chart) {
    chart.yAxis.axisLabel('Tweets');
  };

  var navPages = {
    'Frequency': {
      load: function () {

        requestView('dayhour', function (result) {
          console.log('dayhour', result);
//          result.rows = [{
//            key: 'Tue00'
//          }];
          var points = [];
          var formattedMap = {};
          result.rows.forEach(function (result) {
            // TODO(aramk) we should use an array key and avoid needing to parse a string
            var match = result.key.match(/([a-z]+)(\d+)/i);
            var dayName = match[1], hourNumber = parseInt(match[2]);
            var point = dayHourValue(dayName, hourNumber);
            formattedMap[point] = dayName + ' ' + hourNumber + 'h';
            points.push({key: point, value: result.value});
          });

          drawLineChart('#chart-dayhour svg', {rows: points}, {
            construct: function (chart) {
              chart.xAxis.axisLabel('Daily Hours').tickFormat(function (point) {
                return formattedMap[point];
              });
              chart.yAxis.axisLabel('Tweets');
            }
          });
        });

        requestView('hours', function (result) {
          drawLineChart('#chart-hours svg', result, {
            construct: function (chart) {
              chart.xAxis.axisLabel('Hours').tickFormat(function (point) {
                return point + 'h';
              });
              chart.yAxis.axisLabel('Tweets');
            }
          });
        });

      }
    },
    'Time' : {
      load: function () {
        requestView('daytrend', function (result) {
          var monthNum = {
            'Jan': 01,
            'Feb': 02,
            'Mar': 03,
            'Apr': 04,
            'May': 05,
            'Jun': 06,
            'Jul': 07,
            'Aug': 08,
            'Sep': 09,
            'Oct': 10,
            'Nov': 11,
            'Dec': 12
          };


          var data = [];
          result.rows.map(function (row) {
            var month, day;
            month = monthNum[row.key.substring(0, 3)];
            day = parseInt(row.key.substring(4, 6));
            var date = new Date(2013,month-1, day);
            console.log('data', row.key);
            console.log('month', month);
            console.log('day', day);
            console.log('date', date.toLocaleString());
            data.push({
              x: date,
              y: row.value
            });
          });

          data.sort(function (a, b) {
            if (a.x > b.x) {
              return 1;
            } else if (a.x < b.x) {
              return -1;
            } else {
              return 0;
            }
          });



          var chartData = {
            values: data,
            key: 'Tweets'
          };

          drawLineChart('#chart-time svg', [chartData], {
            processed: true,
            construct: function (chart) {
              chart.yAxis.axisLabel('Tweets');
//              chart.yAxis.scale().domain([0, 100]); // Shows at least 100
              chart.forceY([0, 100]);
              chart.xAxis.axisLabel('Day').tickFormat(function (point) {
                var date = new Date(point);
                var asarray = date.toLocaleString().split('/');
                //return date.getDay() + '/' + date.getMonth();
                return asarray[0] + '/' + asarray[1] + '/13';
              });
            }
          });

        });
      }
    },
    'Location': {
      load: function () {
        var mapOptions = {
          zoom: 12,
          center: new google.maps.LatLng(location[0], location[1]),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map($('#map-location')[0], mapOptions);

//        var bboxCoords = bboxdsPoly.map(function (coord) {
//          return new google.maps.LatLng(coord[0], coord[1]);
//        });

        var bboxCoords = new google.maps.LatLngBounds(
            new google.maps.LatLng(bbox[0][0], bbox[0][1]),
            new google.maps.LatLng(bbox[1][0], bbox[1][1]));

//        var flightPath = new google.maps.Polyline({
//          path: bboxCoords,
//          geodesic: true,
//          fillColor: '#fff',
//          fillOpacity: 0.5,
//          strokeColor: '#ff4a4a',
//          strokeOpacity: 1.0,
//          strokeWeight: 2
//        });
//        flightPath.setMap(map);

        var bboxRect = new google.maps.Rectangle({
          bounds: bboxCoords,
          fillColor: '#fff',
          fillOpacity: 0.5,
          strokeColor: '#ff4a4a',
          strokeOpacity: 1.0,
          strokeWeight: 2,
          map: map,
          zIndex: 0
        });

        requestView('locations', function (result) {
          console.error('locations', result.rows);
          var valuesDist = {};
          var valuesCumulativeDist = {};

          result.rows.forEach(function (point, i) {
            var value = point.value;
            valuesDist[value] = valuesDist[value] || 0;
            valuesDist[value] += 1;
          });

          var values = Object.keys(valuesDist).map(function (key) {
            return parseInt(key);
          });
          values.sort(function (a, b) {
            return a - b;
          });
          console.error('values', values);
          // Accumulate the values for each values.
          values.forEach(function (value, i) {
            valuesCumulativeDist[value] = valuesDist[value];
            for (var j = 0; j < i; j++) {
              valuesCumulativeDist[value] += valuesDist[values[j]];
            }
          });

          var max = valuesCumulativeDist[values[values.length - 1]];

          console.error('max', max);
          console.error('valuesDist', valuesDist);
          console.error('valuesCumulativeDist', valuesCumulativeDist);
          result.rows.forEach(function (point) {
            var coord = point.key;
            var value = point.value;
            // TODO(aramk) relax this a bit and see if it's better
            var radius = (Math.log(value * 2) * 30) + 10;
            var ratio = valuesCumulativeDist[value] / max;
//            console.error('value, ratio', value, valuesCumulativeDist[value], max, ratio);
            var colorHsv = { h: (180 - 180 * ratio), s: 1, v: 1 };
            var colorHex = tinycolor(colorHsv).toHexString();
//            console.error('radius', radius, value / max, colorHsv, colorHex);
            var populationOptions = {
              strokeColor: '#000',
              strokeOpacity: 0.8,
              strokeWeight: 1,
              fillColor: colorHex,
              fillOpacity: 1,
              map: map,
              center: new google.maps.LatLng(coord[1], coord[0]),
              radius: radius,
              zIndex: 100
//              radius: 5 + Math.log(10 * population)/Math.log(2)
            };
            // Add the circle for this city to the map.
            var circle = new google.maps.Circle(populationOptions);
          });
//          for (var city in citymap) {
//            var populationOptions = {
//              strokeColor: '#FF0000',
//              strokeOpacity: 0.8,
//              strokeWeight: 2,
//              fillColor: '#FF0000',
//              fillOpacity: 0.35,
//              map: map,
//              center: citymap[city].center,
//              radius: citymap[city].population / 20
//            };
//            // Add the circle for this city to the map.
//            cityCircle = new google.maps.Circle(populationOptions);
//          }
        });
      }
    },
    'Languages': {
      load: function () {
        requestView('languages', function (result) {
          $.getJSON(Window.currentURL() + 'js/langs.json', function (langs) {
            console.error('langs', langs);
            drawHorizontalBarChart('#chart-languages svg', result, {
              construct: function (chart) {
                chart.xAxis.tickFormat(function (lang) {
                  if (lang == 'in') {
                    // Indian is incorrectly labelled a language instead of Hindi.
                    lang = 'hi';
                  }
                  if (lang == 'und' || !lang) {
                    return 'N/A'
                  } else {
                    return langs[lang] ? langs[lang].name : lang;
                  }
                });
                chart.yAxis.axisLabel('Tweets');
                chart.showLegend(false);
              },
              height: 800,
              padding: 80
            });
          });
        });
      }
    },
    'Visitors': {
      load: function () {
        requestView('visitors', function (result) {
          drawBarChart('#chart-visitors svg', result, {
            construct: tweetYAxis
          });
        });
      }
    },
    'Sentiments': {
      load: function () {
        requestView('sentiment', function (result) {
          drawBarChart('#chart-sentiment svg', result, {
            construct: tweetYAxis
          });
        });

        requestView('sentiment_time', function (result) {
          console.error('sentiment_time', result);
          var sentiments = {}, formattedMap = {};
          result.rows.forEach(function (row) {
            var type = row.key[0];
            sentiments[type] = sentiments[type] || [];

            var dayName = row.key[1], hourNumber = row.key[2];
            var point = dayHourValue(dayName, hourNumber);
            formattedMap[point] = dayName + ' ' + hourNumber + 'h';

            sentiments[type].push({
              x: point,
              y: row.value
            });
          });

          var series = [];
          for (var mood in sentiments) {
            series.push({
              values: sentiments[mood],
              key: mood
            });
          }

          drawLineChart('#chart-sentiment-time svg', series, {
            processed: true,
            construct: function (chart) {
              chart.showLegend(true);
              chart.yAxis.axisLabel('Tweets');
              chart.xAxis.axisLabel('Daily Hours').tickFormat(function (point) {
                return formattedMap[point];
              });
            }
          });
        });
      }
    },
    'Stalkers': {
      load: function () {
        console.log('requesting view');
        requestView('users', function (result) {
          // Build key:value hash

          var winners = pickTopN(10, result.rows, 'value');
          for (var i = 0; i < winners.length; i++) {
            winners[i]['label'] = winners[i].key;
          }

          winners.sort(function (a, b) {
            if (a.label == b.label) {
              return 0;
            } else if (a.label > b.label) {
              return 1;
            } else {
              return -1;
            }

          });

          var chartData = {
            key: 'Top Tweeters',
            values: winners
          };
          drawBarChart('#chart-stalkers svg', chartData, {processed: true, construct: tweetYAxis});
        });
      }
    },
    'Distribution': {
      load: function () {
        console.log('requestion users');
        requestView('users', function (result) {
          var users = {};
          result.rows.map(function (item) {
            users[item.key] = item.value;
          });
          console.log('calculation distribution');

          var userValues = [];
          for (var id in users) {
            userValues.push(users[id]);
          }

          var datapoints = normalDistDataPoints(userValues);

          var chartData = {
            values: datapoints,
            key: 'Normal Distribution'
          };

          drawLineChart('#chart-distribution svg', [chartData], {
            processed: true,
            construct: function (chart) {
              chart.yAxis.axisLabel('Pr (Tweets)').tickFormat(function (point) {
                return point.toFixed(4);
              });
              chart.xAxis.axisLabel('Tweets').tickFormat(function (point) {
                return point.toFixed(4);
              });
            }
          });
        });
      }
    },
    'Hashtags': {
      load: function () {
        requestView('hashtags', function (result) {
          console.log('retrieved view hashtags');

          // Eliminate duplicates (Case insensitive)
          var tags = {}; // Maps lower case tags to the cased equiv
          for(var i = 0; i < result.rows.length; i++) {
            var row = result.rows[i];
            var index = i;
            if (tags[row.key.toLowerCase()]) {
              tags[row.key.toLowerCase()].value += row.value;
              console.log('adding to index', index);
              var toupdate = result.rows.indexOf(tags[row.key.toLowerCase()].item);
              result.rows[toupdate].value = tags[row.key.toLowerCase()].value;
              console.log('updating index', toupdate);
              if (index >= 0) {
                result.rows.splice(index, 1);
                i--;
              } else {
                console.error('could not find row', row);
              }
            } else {
              tags[row.key.toLowerCase()] = {
                capitalised: row.key,
                value: row.value,
                item: row
              };
            }
          }
          var winners = pickTopN(10, result.rows, 'value');

          console.log('winners', winners);

          for (var i = 0; i < winners.length; i++) {
            winners[i]['label'] = winners[i].key;
          }

          winners.sort(function (a, b) {
            if (a.label == b.label) {
              return 0;
            } else if (a.label > b.label) {
              return 1;
            } else {
              return -1;
            }

          });

          var chartData = {
            key: 'Top Tweeters',
            values: winners
          };

          drawBarChart('#chart-hashtags svg', chartData, {processed: true, construct: tweetYAxis});
        });

        requestView('hashtags_count', function (result) {
          drawBarChart('#chart-hashtags-count svg', result, {
            construct: function (chart) {
              chart.yAxis.axisLabel('Tweets with # Hashtags');
              chart.xAxis.axisLabel('Hashtags');
            }
          });
        });
      }
    },

    'Trends': {
      load: function () {
        requestView('hashtags', function (result) {


          var tags = {}; // Maps lower case tags to the cased equiv
          for(var i = 0; i < result.rows.length; i++) {
            var row = result.rows[i];
            var index = i;
            if (tags[row.key.toLowerCase()]) {
              tags[row.key.toLowerCase()].value += row.value;
              console.log('adding to index', index);
              var toupdate = result.rows.indexOf(tags[row.key.toLowerCase()].item);
              result.rows[toupdate].value = tags[row.key.toLowerCase()].value;
              console.log('updating index', toupdate);
              if (index >= 0) {
                result.rows.splice(index, 1);
                i--;
              } else {
                console.error('could not find row', row);
              }
            } else {
              tags[row.key.toLowerCase()] = {
                capitalised: row.key,
                value: row.value,
                item: row
              };
            }
          }

          var winners = pickTopN(10, result.rows, 'value');

          for (var i = 0; i < winners.length; i++) {
            winners[i]['label'] = winners[i].key;
          }

          winners.sort(function (a, b) {
            if (a.label == b.label) {
              return 0;
            } else if (a.label > b.label) {
              return 1;
            } else {
              return -1;
            }
          });

          console.log('requesting tagtime');
          requestView('tagtime', function (result) {
            console.log('recieved tagtime');
            var series = {};
            var cumulative = {};

          var tags = {}; // Maps lower case tags to the cased equiv
          for(var i = 0; i < result.rows.length; i++) {
            var row = result.rows[i];
            var index = i;
            if (tags[row.key.tag.toLowerCase()]) {
              result.rows[i].key.tag = tags[row.key.tag.toLowerCase()].capitalised;
            } else {
              tags[row.key.tag.toLowerCase()] = {
                capitalised: row.key.tag
              };
            }
          }


            var init = Date.parse(result.rows[0].key.time);

            // Keep track of the max and min dates.
            var minDate = init, maxDate = init;

            result.rows.map(function (item) {
              var key = item.key.tag.toLowerCase();
              if (!series[key]) {
                series[key] = [];
                cumulative[key] = 0;
              }

              var date = Date.parse(item.key.time);
              if (date > maxDate) {
                maxDate = date;
              } else if (date < minDate) {
                minDate = date;
              }
              cumulative[key] += item.value;
              series[key].push({x: date, y: item.value});//cumulative[item.key.tag]});

            });

            var compare = function (a, b) {
              return a.x - b.x;
            };
            for (var key in series) {
              series[key].sort(compare);
              var sum = 0;
              for(var i = 0; i < series[key].length; i++) {
                sum += series[key][i].y;
                series[key][i].y = sum;
              }
            }

            var chartData = [];
            /*var chartData = {
             values: datapoints,
             key: 'Normal Distribution',
             color: '#000'
             };*/

            for (var i = 0; i < winners.length; i++) {
              var key = winners[i].label.toLowerCase();
              var maxVal = series[key][series[key].length - 1].y;
              series[key].unshift({x: minDate, y: 0});
              series[key].push({x: maxDate, y: maxVal});
              chartData.push({
                values: series[key],
                key: winners[i].label
              });
            }

            console.log('drawing trend chart');

            drawLineChart('#chart-trends svg', chartData, {processed: true, legend: true, construct: function (chart) {
              chart.xAxis.axisLabel('Daily Hours').tickFormat(function (point) {
                var date = new Date(point);
                  return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                });
                chart.yAxis.axisLabel('Tweets');
            }});

          });

        });
      }
    },

    'Mentions': {
      load: function () {
        requestView('mentions_count', function (result) {
          drawBarChart('#chart-mentions-count svg', result, {
            construct: function (chart) {
              chart.yAxis.axisLabel('Tweets with # Mentions');
              chart.xAxis.axisLabel('Mentions');
            }
          });
        });

        requestView('mentions', function (result) {
          result.rows.sort(function (a, b) {
            return b.value - a.value;
          });
          result.rows = result.rows.slice(0, 10);
          drawBarChart('#chart-mentions svg', result, {
            construct: function (chart) {
              chart.yAxis.axisLabel('Mentions');
              chart.xAxis.axisLabel('User');
            }
          });
        });
      }
    },
    'Profanity': {
      load: function () {
        console.log('requesting view');
        requestViewUngrouped('wordcount', function (wordCount) {
          requestView('profanity', function (result) {

            // Build key:value hash
            var profanities = pickTopN(result.rows.length, result.rows, 'value');
            var numCusses = 0;
            for (var i = 0; i < profanities.length; i++) {
              profanities[i]['label'] = profanities[i].key;
              numCusses += profanities[i].value;
            }
            /*
             profanities.push({
             label: 'Non-cusswords',
             value: wordCount.rows[0].value - numCusses
             });
             */

            var chartData = {
              key: 'Top Profanities',
              values: profanities
            };
            console.log('Words', wordCount.rows[0].value);
            console.log(chartData);
            drawBarChart('#chart-profanity svg', chartData, {
              processed: true,
              construct: function (chart) {
                chart.yAxis.axisLabel('Tweets');
                chart.xAxis.axisLabel('Words');
              }
            });
            console.log('Top profanities:', chartData);

          });
        });

        requestView('profanity_2', function (result) {
          drawBarChart('#chart-profanity-2 svg', result, {
            construct: function (chart) {
              chart.yAxis.axisLabel('Tweets');
              chart.xAxis.axisLabel('Words');
            }
          });
        });

        requestView('profanity_2_days', function (result) {
          var words = {}, formattedMap = {};
          result.rows.forEach(function (row) {
            var word = row.key[0];
            words[word] = words[word] || [];

            var dayName = row.key[1], hourNumber = row.key[2];
            var point = dayHourValue(dayName, hourNumber);
            formattedMap[point] = dayName + ' ' + hourNumber + 'h';

            words[word].push({
              x: point,
              y: row.value
            });
          });

          var series = [];
          for (var word in words) {
            series.push({
              values: words[word],
              key: word
            });
          }

          drawLineChart('#chart-profanity-2-days svg', series, {
            processed: true,
            construct: function (chart) {
              chart.showLegend(true);
              chart.yAxis.axisLabel('Tweets');
              chart.xAxis.axisLabel('Daily Hours').tickFormat(function (point) {
                return formattedMap[point];
              });
            }
          });

        });
      }
    }
  };
  for (var pageName in navPages) {
    (function (pageName, pageObj) {
      var pageItem = $('<li><a>' + pageName + '</a></li>');
      pageItem.on('click', function () {
        $('#page-header').html(pageName);
        var pageId = nameToID(pageName);
        console.log('pageId', pageId);
        $('#content > div').hide();
        $('#page-' + pageId).show();
        setTimeout(function () {
          if (!pageObj._loaded && pageObj.load) {
            pageObj.load();
            pageObj._loaded = true;
          }
        }, 0);
      });
      $('#nav-list').append(pageItem);
    })(pageName, navPages[pageName]);
  }
  var navButtons = $('.bs-sidenav a');
  navButtons.first().trigger('click');

});


